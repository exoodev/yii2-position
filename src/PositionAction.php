<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\position;

use Yii;
use yii\web\BadRequestHttpException;

/**
 * Position action.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class PositionAction extends Action
{
    /**
     * @var string the property
     */
    public $afterMove;

    /**
     * Changing the status of the [[Model]] model on the opposite.
     * @param string $id Model id.
     * @return mixed
     */
    public function run($move, $id, $position = false)
    {
        if (!Yii::$app->request->isPost) {
            throw new BadRequestHttpException('Only POST is allowed');
        }

        $model = $this->findModel($id);

        $actions = [
            'prev' => 'movePrev',
            'next' => 'moveNext',
            'first' => 'moveFirst',
            'last' => 'moveLast',
            'position' => 'moveToPosition',
        ];

        if (isset($actions[$move])) {
            $move = $actions[$move];
            $model->$move($position);
        }

        if ($this->afterMove !== null) {
            call_user_func($this->afterMove, $this);
        }

        if (!Yii::$app->request->isAjax) {
            return Yii::$app->getResponse()->redirect(Yii::$app->getUser()->getReturnUrl());
        } else {
            return $this->controller->asJson(['result' => true]);
        }
    }
}
